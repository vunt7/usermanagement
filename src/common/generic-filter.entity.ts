import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, Min } from 'class-validator';
import { FindOptionsWhere, ILike } from 'typeorm';

import { Group } from 'src/modules/group/group.entity';
import { User } from 'src/modules/user/user.entity';

export enum SortOrder {
    ASC = 'ASC',
    DESC = 'DESC',
}

export interface IPaginationOptions {
    page?: number;
    pageSize?: number;
}

export interface ISortingOptions {
    orderBy?: string;
    sortOrder?: SortOrder;
    createOrderQuery(): object;
}

export interface IFilterOptions<T> {
    createQuery(): FindOptionsWhere<T>;
}

export const hasPaginationOptions = (value: IPaginationOptions): value is IPaginationOptions => !!value.page;

export const hasSortingOptions = (value: ISortingOptions): value is ISortingOptions => !!value.orderBy;

export class GroupFilter implements IPaginationOptions, ISortingOptions, IFilterOptions<Group> {
    @Type(() => Number)
    @IsOptional()
    @IsNumber({}, { message: ' "page" atrribute should be a number' })
    page?: number = 1;

    @Type(() => Number)
    @IsOptional()
    pageSize?: number = 10;

    @IsOptional()
    orderBy?: string = 'id';

    @IsOptional()
    @IsEnum(SortOrder)
    sortOrder?: SortOrder = SortOrder.ASC;

    @IsOptional()
    name?: string = '';

    createQuery(): FindOptionsWhere<Group> {
        const where: FindOptionsWhere<Group> = {};

        where.name = ILike(`%${this.name}%`);

        return where;
    }

    createOrderQuery(): object {
        const order = {};
        order[this.orderBy] = this.sortOrder;
        return order;
    }
}

export class UserFilter implements IPaginationOptions, ISortingOptions, IFilterOptions<User> {
    @IsNumber({}, { message: ' "page" atrribute should be a number' })
    @Type(() => Number)
    page?: number = 1;

    @IsNumber({}, { message: ' "pageSize" attribute should be a number ' })
    @Type(() => Number)
    pageSize?: number = 10;

    @IsOptional()
    orderBy?: string = 'id';

    @IsOptional()
    @IsEnum(SortOrder)
    sortOrder?: SortOrder = SortOrder.ASC;

    @IsOptional()
    username?: string = '';

    @IsOptional()
    email?: string = '';

    createQuery(): FindOptionsWhere<User> {
        const where: FindOptionsWhere<User> = {};

        where.username = ILike(`%${this.username}%`);

        where.email = ILike(`%${this.email}%`);

        return where;
    }

    createOrderQuery(): object {
        const order = {};

        if (this.orderBy) order[this.orderBy] = this.sortOrder;

        return order;
    }
}
