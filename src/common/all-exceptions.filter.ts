import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { Response } from 'express';

interface IExceptionType {
    statusCode: number;
    path: unknown;
    message?: string;
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

    catch(exception: unknown, host: ArgumentsHost): void {
        // In certain situations `httpAdapter` might not be available in the
        // constructor method, thus we should resolve it here.
        const { httpAdapter } = this.httpAdapterHost;

        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const httpStatus: number =
            exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        const responseBody: IExceptionType = {
            statusCode: httpStatus,
            path: httpAdapter.getRequestUrl(ctx.getRequest()),
        };
        if (exception['response']?.message) responseBody.message = exception['response'].message;

        // httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
        response.status(httpStatus).json({
            ...responseBody,
        });
    }
}
