import { FindManyOptions, Repository } from 'typeorm';

import { IFilterOptions, IPaginationOptions, ISortingOptions } from './generic-filter.entity';
import { Pagination } from './pagination.interface';

export class PaginationRepository<T> extends Repository<T> {
    async paginate(
        filter: IPaginationOptions & ISortingOptions & IFilterOptions<T>,
        options?: FindManyOptions<T>,
    ): Promise<Pagination<T>> {
        const queryOptions: FindManyOptions<T> = {
            ...options,
            order: filter?.createOrderQuery(),
            skip: Math.max(+filter.pageSize, 1) * (Math.max(+filter.page, 1) - 1),
            take: Math.max(filter.pageSize, 1),
            where: filter.createQuery(),
        };

        const res: [T[], number] = await this.findAndCount(queryOptions);
        return {
            items: res[0],
            totalElements: res[1],
            totalPages: Math.floor((res[1] + Math.max(+filter.pageSize, 1) - 1) / Math.max(+filter.pageSize, 1)),
            currentPage: filter.page,
            pageSize: filter.pageSize,
        };
    }
}
