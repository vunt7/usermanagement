export interface Pagination<T> {
    items: T[];
    totalElements: number;
    totalPages: number;
    currentPage: number;
    pageSize: number;
}
