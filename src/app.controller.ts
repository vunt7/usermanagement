import { Controller, Get } from '@nestjs/common';
import { Public } from './modules/auth/decorator/auth.decorator';

@Controller()
export class AppController {
    constructor() {}

    @Public()
    @Get()
    getHello(): string {
        return 'Welcome to user management service, please login to use this service!!!';
    }
}
