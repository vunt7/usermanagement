import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class EmailService {
    private transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // Use `true` for port 465, `false` for all other ports
        auth: {
            user: process.env.EMAIL_NAME,
            pass: process.env.EMAIL_APP_PASSWORD,
        },
    });

    // send mail with defined transport object
    async sendEmail(email: string, subject: string, html: unknown) {
        const mailOptions = {
            from: '"mimingucci 👻" <no-reply@mimingucci.email>', // sender address
            to: email, // list of receivers
            subject: subject, // Subject line
            html: html, // html body
        };

        return this.transporter.sendMail(mailOptions);
    }
}
