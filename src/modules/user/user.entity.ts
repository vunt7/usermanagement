import { Exclude } from 'class-transformer';
import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';

import { Group } from 'src/modules/group/group.entity';
import { Message } from 'src/modules/message/message.entity';
import { Room } from '../room/room.entity';

@Entity('users')
@Unique(['email'])
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar' })
    username: string;

    @Column({ type: 'varchar', unique: true })
    email: string;

    @Column({ type: 'varchar' })
    @Exclude()
    password: string;

    @Column({ default: false })
    enabled: boolean;

    @ManyToMany(() => Group, (group: Group) => group.members)
    groups: Group[];

    @ManyToMany(() => Room, (room: Room) => room.members)
    rooms: Room[];

    @OneToMany(() => Message, (message: Message) => message.author, { cascade: true, onDelete: 'CASCADE' })
    messages: Message[];

    @Column({
        type: 'varchar',
        nullable: true,
        default: 'public\\img\\default\\default-avatar.jpg',
    })
    avatar: string;

    @Column({ name: 'reset_token', type: 'varchar', nullable: true, default: '' })
    reset_token?: string;

    @Column({ name: 'activation_key', type: 'varchar', nullable: true })
    activation_key?: string;

    @Column({ name: 'last_visit', type: 'date', nullable: true })
    last_visit?: Date;
}
