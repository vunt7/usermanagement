import { DataSource, FindManyOptions } from 'typeorm';
import { Injectable } from '@nestjs/common';

import { PaginationRepository } from 'src/common/pagination.repository';
import { User } from './user.entity';
import { IPaginationOptions, ISortingOptions, IFilterOptions } from 'src/common/generic-filter.entity';
import { Pagination } from 'src/common/pagination.interface';

@Injectable()
export class UserRepository extends PaginationRepository<User> {
    constructor(private dataSource: DataSource) {
        super(User, dataSource.createEntityManager());
    }

    paginate(
        filter: IPaginationOptions & ISortingOptions & IFilterOptions<User>,
        options?: FindManyOptions<User>,
    ): Promise<Pagination<User>> {
        return super.paginate(filter, options);
    }
}
