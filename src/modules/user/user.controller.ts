import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Param,
    ParseIntPipe,
    Post,
    Put,
    Query,
    Res,
    UseInterceptors,
    UploadedFile,
    Delete,
    Patch,
    Req,
} from '@nestjs/common';
import { Response } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ApiBody, ApiConsumes, ApiHeader, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtService } from '@nestjs/jwt';
import { existsSync, mkdirSync } from 'fs';

import { UserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import { User } from './user.entity';
import { RequirePermissions } from 'src/modules/auth/decorator/permissions.decorator';
import { PERMISSION } from 'src/modules/group/group.entity';
import { Pagination } from 'src/common/pagination.interface';
import { UpdateUserDTO } from './dto/update-user.dto';
import { UserFilter } from 'src/common/generic-filter.entity';

@Controller('users')
@ApiResponse({ status: 403, description: 'Forbidden.' })
@ApiTags('Users')
@ApiHeader({ name: 'Authorization' })
export class UserController {
    constructor(
        private userService: UserService,
        private jwtService: JwtService,
    ) {}

    @Get()
    @ApiResponse({ status: HttpStatus.OK, description: 'Get all user info' })
    async findAll(@Req() req: Request, @Res() res: Response, @Query() filter: UserFilter) {
        const users: Pagination<User> = await this.userService.findAllPaginated(filter);
        return res.status(200).json({
            data: users,
            statusCode: HttpStatus.OK,
            message: 'Fetch all users successfully',
            requestedBy: req['user'].id,
        });
    }

    @Get(':id')
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: 'Cannot find user with provided id',
    })
    @ApiResponse({ status: HttpStatus.OK, description: 'Request accepted' })
    async findById(@Req() req: Request, @Res() res: Response, @Param('id', ParseIntPipe) id: number) {
        const user: UserDTO = await this.userService.findOne(id);
        return res.status(200).json({
            data: user,
            message: `Find user with id ${id} successfully`,
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Post(':id/avatar')
    @RequirePermissions(PERMISSION.EDIT_ACCOUNT)
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: 'Cannot find user with provided id',
    })
    @ApiResponse({
        status: HttpStatus.CREATED,
        description: 'Upload avatar successfully',
    })
    @ApiConsumes('multipart/form-data')
    @ApiBody({
        required: true,
        schema: {
            type: 'object',
            properties: {
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                // destination: 'public/img',
                destination: (req, file, callback) => {
                    const uploadPath = `public/img/${req.params.id}`; // Define destination folder here
                    if (!existsSync(uploadPath)) {
                        mkdirSync(uploadPath, { recursive: true });
                    }
                    // unlinkSync to delete older avatar
                    callback(null, uploadPath);
                },
                filename: (req, file, callback) => {
                    const randomName = Array(32)
                        .fill(null)
                        .map(() => Math.round(Math.random() * 16).toString(16))
                        .join('');
                    callback(null, `${randomName}${file.originalname}`);
                },
            }),
        }),
    )
    async uploadAvatar(
        @Res() res: Response,
        @UploadedFile() file: Express.Multer.File,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const savedUser: UserDTO = await this.userService.findOne(id);
        savedUser.avatar = file.path;
        const updatedUser = await this.userService.update(id, savedUser);
        return res.status(200).json({
            data: updatedUser,
            statusCode: HttpStatus.OK,
            message: 'Upload avatar successfully',
        });
    }

    @Delete(':id/avatar')
    @RequirePermissions(PERMISSION.EDIT_ACCOUNT)
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: 'Cannot find user with provided id',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Unset avatar successfully',
    })
    async unsetAvatar(@Req() req: Request, @Res() res: Response, @Param('id', ParseIntPipe) id: number) {
        const userDto = new UserDTO({});
        userDto.avatar = 'public\\img\\default\\default-avatar.jpg';
        const updatedUser = await this.userService.update(id, userDto);
        return res.status(200).json({
            data: updatedUser,
            message: 'Unset avatar successfully',
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Put(':id')
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: 'Cannot find user with provided id',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Update user successfully',
    })
    async update(
        @Req() req: Request,
        @Res() res: Response,
        @Body() userDto: UpdateUserDTO,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const updatedUser = await this.userService.update(id, userDto);
        //generate new jwt respect to changed data
        const accessToken = await this.jwtService.signAsync({ id: updatedUser.id, username: updatedUser.username });
        return res.status(200).json({
            data: updatedUser,
            // accessToken,
            message: 'Update user successfully',
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Patch()
    @RequirePermissions(PERMISSION.EDIT_ACCOUNT)
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: 'Cannot find user with provided id',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Update user successfully',
    })
    async activeAccount(
        @Req() req: Request,
        @Res() res: Response,
        @Body() payload: { id: number; username: string; enabled: boolean },
    ) {
        const result: UserDTO = await this.userService.activeAccount(payload);
        return res.status(200).json({
            data: result,
            message: 'Update user successfully',
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }
}
