import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

import { IUser } from '../interfaces/user.interface';

//class for creating new account
export class SignupUserDTO implements IUser {
    @ApiProperty({
        description: 'Email is unique for each account',
    })
    @IsEmail()
    email: string;

    @ApiProperty({
        description: 'Username can be the same for other accounts',
    })
    @IsString()
    @IsNotEmpty()
    username: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    password: string;
}
