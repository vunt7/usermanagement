import { IsArray, IsEmail, IsInt, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

import { GroupDTO } from 'src/modules/group/dto/group.dto';
import { IUser } from '../interfaces/user.interface';

export class UserDTO implements IUser {
    @IsInt()
    id: number;

    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString()
    avatar: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @Exclude()
    password: string;

    @Exclude()
    access_token: string;

    @Exclude()
    activation_key: string;

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => GroupDTO)
    groups?: GroupDTO[] = [];

    constructor(partial: Partial<UserDTO>) {
        Object.assign(this, partial);
    }
}
