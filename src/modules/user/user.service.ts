import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { FindManyOptions } from 'typeorm';
import { plainToInstance } from 'class-transformer';
import * as bcrypt from 'bcrypt';

import { SignupUserDTO } from './dto/signup-user.dto';
import { UserDTO } from './dto/user.dto';
import { User } from './user.entity';
import { UpdateUserDTO } from './dto/update-user.dto';
import { UserFilter } from 'src/common/generic-filter.entity';
import { UserRepository } from './user.repository';

export interface IFilterFields {
    username: string;
    email: string;
}

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository) {}

    // generate random string
    generateString(length: number): string {
        // declare all characters
        const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let result: string = '';
        const charactersLength: number = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }

    async create(userDTO: SignupUserDTO): Promise<User> {
        const user: User = new User();
        user.username = userDTO.username;
        user.email = userDTO.email;
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(userDTO.password, salt);
        user.activation_key = this.generateString(100);
        return this.userRepository.save(user);
    }

    findAll(): Promise<User[]> {
        return this.userRepository.find({ select: ['id', 'username', 'email', 'enabled', 'avatar'] });
    }

    async findOne(id: number): Promise<UserDTO> {
        const user = await this.userRepository.findOne({
            where: { id },
            relations: ['groups'],
        });
        if (!user) throw new NotFoundException(`Cannot find user with id ${id}`);
        return plainToInstance(UserDTO, user);
    }

    async findOneByEmail(email: string): Promise<User | null> {
        const user = await this.userRepository.findOneBy({ email });
        return user;
    }

    async emailExists(email: string): Promise<boolean> {
        return this.userRepository.exists({ where: { email } });
    }

    async update(id: number, userDto: UpdateUserDTO): Promise<UserDTO> {
        const existUser: User = await this.userRepository.findOneBy({
            id,
        });
        if (!existUser) throw new NotFoundException(`Cannot find user with id ${id}`);
        const salt = await bcrypt.genSalt();
        if (userDto?.password) userDto.password = await bcrypt.hash(userDto.password, salt);
        Object.assign(existUser, userDto);
        const updatedUser = await this.userRepository.save(existUser);
        return plainToInstance(UserDTO, updatedUser);
    }

    async activeAccount(payload: { id: number; username: string; enabled: boolean }): Promise<UserDTO> {
        const user: User = await this.userRepository.findOne({
            where: { id: payload?.id, username: payload?.username },
        });
        if (!user) throw new NotFoundException(`Token is not valid`);
        user.enabled = payload.enabled; // active account
        const updatedUser = await this.userRepository.save(user); // update account
        return plainToInstance(UserDTO, updatedUser);
    }

    async verifyUser(payload: { email: string; key: string }): Promise<UserDTO> {
        const user: User = await this.userRepository.findOne({ where: { email: payload.email } });
        if (!user || !user.activation_key || user.activation_key !== payload.key)
            throw new NotFoundException(`Cannot find user with email ${payload.email}`);
        user.enabled = true; // active account
        user.activation_key = ''; // remove active key
        const updatedUser = await this.userRepository.save(user); // update account
        return plainToInstance(UserDTO, updatedUser);
    }

    async findAllPaginated(filter: UserFilter) {
        const options: FindManyOptions<User> = {
            relations: ['groups'],
            select: ['id', 'username', 'email', 'avatar', 'enabled'],
        };
        return await this.userRepository.paginate(filter, options);
    }

    async forgotPassword(email: string): Promise<string> {
        const user: User = await this.userRepository.findOne({ where: { email } });
        if (!user) throw new NotFoundException(`Cannot find user with email: ${email}`);
        user.reset_token = this.generateString(15);
        const updatedUser = await this.userRepository.save(user);
        return updatedUser.reset_token;
    }

    async resetPassword(payload: { email: string; password: string; resetToken: string }): Promise<boolean> {
        const user: User = await this.userRepository.findOne({ where: { email: payload.email } });
        if (!user) throw new NotFoundException(`Cannot find user with email ${payload.email}`);
        if (user.reset_token !== payload.resetToken) throw new BadRequestException(`Reset key is not correct`);
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(payload.password, salt); // hash new password
        user.reset_token = ''; // reset resetPassword
        await this.userRepository.save(user);
        return true;
    }
}
