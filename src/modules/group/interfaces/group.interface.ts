import { PERMISSION } from '../group.entity';

export interface IGroup {
    name: string;

    description?: string;

    permissions: PERMISSION[];

    enabled?: boolean;
}
