import { Injectable } from '@nestjs/common';
import { DataSource, FindManyOptions } from 'typeorm';

import { PaginationRepository } from 'src/common/pagination.repository';
import { IPaginationOptions, ISortingOptions, IFilterOptions } from 'src/common/generic-filter.entity';
import { Pagination } from 'src/common/pagination.interface';
import { Group } from './group.entity';

@Injectable()
export class GroupRepository extends PaginationRepository<Group> {
    constructor(private dataSource: DataSource) {
        super(Group, dataSource.createEntityManager());
    }

    paginate(
        filter: IPaginationOptions & ISortingOptions & IFilterOptions<Group>,
        options?: FindManyOptions<Group>,
    ): Promise<Pagination<Group>> {
        return super.paginate(filter, options);
    }
}
