import { IsArray, IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { PERMISSION } from '../group.entity';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IGroup } from '../interfaces/group.interface';

//used for creating new group
export class CreateGroupDTO implements IGroup {
    @ApiProperty({
        description: 'Name of the group',
    })
    @IsNotEmpty()
    name: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    description?: string;

    @ApiPropertyOptional({
        description: 'If true, the group will be active',
    })
    @IsOptional()
    @IsBoolean()
    enabled?: boolean;

    @ApiPropertyOptional({
        type: [Number],
        description: 'Is an array including the id of each member',
    })
    @IsOptional()
    @IsArray()
    @IsNumber({}, { each: true })
    members?: number[] = [];

    @ApiProperty({ enum: ['editAccount', 'editGroup'] })
    @IsOptional()
    @IsArray()
    @IsEnum(PERMISSION, { each: true })
    permissions: PERMISSION[];
}
