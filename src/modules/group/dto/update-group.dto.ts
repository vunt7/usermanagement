import { IsArray, IsBoolean, IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { PERMISSION } from '../group.entity';
import { IGroup } from '../interfaces/group.interface';

//used for updating group
export class UpdateGroupDTO implements IGroup {
    @ApiPropertyOptional({
        description: 'Name of the group',
    })
    @IsOptional()
    @IsString()
    name: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    description?: string;

    @ApiPropertyOptional({
        description: 'If true, the group will be active',
    })
    @IsOptional()
    @IsBoolean()
    enabled?: boolean;

    @ApiPropertyOptional({
        type: [Number],
        description: 'Is an array including the id of each member',
    })
    @IsOptional()
    @IsArray()
    @IsNumber({}, { each: true })
    members?: number[];

    @ApiProperty({ enum: ['editAccount', 'editGroup'] })
    @IsOptional()
    @IsArray()
    @IsEnum(PERMISSION, { each: true })
    permissions: PERMISSION[];
}
