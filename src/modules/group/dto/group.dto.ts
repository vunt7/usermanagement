import { IsArray, IsBoolean, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';

import { PERMISSION } from '../group.entity';
import { UserDTO } from 'src/modules/user/dto/user.dto';
import { Type } from 'class-transformer';
import { IGroup } from '../interfaces/group.interface';

export class GroupDTO implements IGroup {
    @IsInt()
    id: number;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    description?: string = '';

    @IsBoolean()
    enabled: boolean;

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => UserDTO)
    members?: UserDTO[] = [];

    @IsOptional()
    @IsArray()
    @IsEnum(PERMISSION, { each: true })
    permissions: PERMISSION[];

    constructor(partial: Partial<GroupDTO>) {
        Object.assign(this, partial);
    }
}
