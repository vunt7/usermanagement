import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

import { User } from 'src/modules/user/user.entity';

export enum PERMISSION {
    EDIT_ACCOUNT = 'editAccount',
    EDIT_GROUP = 'editGroup',
    MANAGE_ROOM = 'manageRoom',
}

@Entity('groups')
export class Group {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar' })
    name: string;

    @Column({ type: 'varchar', nullable: true })
    description?: string;

    @ManyToMany(() => User, (user: User) => user.groups, {
        cascade: ['insert', 'update'],
    })
    @JoinTable({
        name: 'user_group',
        joinColumn: {
            name: 'group_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'user_id',
            referencedColumnName: 'id',
        },
    })
    members: User[];

    @Column('enum', {
        array: true,
        enum: PERMISSION,
        nullable: true,
        default: [],
    })
    permissions: PERMISSION[];

    @Column({ type: 'boolean', nullable: true, default: false })
    enabled: boolean;
}
