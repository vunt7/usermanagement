import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { FindManyOptions } from 'typeorm';
import { plainToInstance } from 'class-transformer';

import { CreateGroupDTO } from './dto/create-group.dto';
import { Group } from './group.entity';
import { User } from 'src/modules/user/user.entity';
import { UpdateGroupDTO } from './dto/update-group.dto';
import { GroupDTO } from './dto/group.dto';
import { GroupRepository } from './group.repository';
import { UserRepository } from '../user/user.repository';
import { GroupFilter } from 'src/common/generic-filter.entity';

@Injectable()
export class GroupService {
    constructor(
        private readonly groupRepo: GroupRepository,
        private readonly userRepo: UserRepository,
    ) {}

    async create(groupDto: CreateGroupDTO): Promise<GroupDTO> {
        try {
            const newGroup = new Group();
            newGroup.name = groupDto.name;
            newGroup.description = groupDto?.description || '';
            newGroup.permissions = groupDto?.permissions || [];
            newGroup.enabled = groupDto?.enabled || false;

            newGroup.members = new Array<User>();
            for (const member of groupDto?.members) {
                const user = await this.userRepo.findOneBy({ id: member });
                if (!user) throw new NotFoundException(`Cannot find user with id ${member}`);
                newGroup.members.push(user);
            }
            return plainToInstance(GroupDTO, this.groupRepo.save(newGroup));
        } catch (error) {
            throw new InternalServerErrorException(error.message);
        }
    }

    findAll(): Promise<Group[]> {
        return this.groupRepo.find();
    }

    async findById(id: number): Promise<GroupDTO> {
        const group = this.groupRepo.findOne({
            where: { id },
            relations: ['members'],
            select: {
                id: true,
                name: true,
                description: true,
                enabled: true,
                permissions: true,
                members: {
                    id: true,
                    username: true,
                    enabled: true,
                    avatar: true,
                    email: true,
                },
            },
        });
        if (!group) throw new NotFoundException(`Cannot find group with id ${id}`);
        return plainToInstance(GroupDTO, group);
    }

    async updateById(id: number, groupDto: UpdateGroupDTO): Promise<GroupDTO> {
        const savedGroup = await this.groupRepo.findOne({ where: { id }, relations: ['members'] });
        if (!savedGroup) throw new NotFoundException(`cannot find group with id ${id}`);
        if (groupDto?.name) savedGroup.name = groupDto.name;
        if (groupDto?.description) savedGroup.description = groupDto.description;
        if (groupDto?.permissions) savedGroup.permissions = groupDto?.permissions;
        if (groupDto?.enabled === true || groupDto?.enabled === false) savedGroup.enabled = groupDto.enabled;

        if (groupDto?.members) {
            savedGroup.members = new Array<User>();
            for (const member of groupDto?.members) {
                const user = await this.userRepo.findOneBy({ id: member });
                if (!user) throw new NotFoundException(`Cannot find user with id ${member}`);
                savedGroup.members.push(user);
            }
        }
        return plainToInstance(GroupDTO, this.groupRepo.save(savedGroup));
    }

    async findAllPaginated(filter: GroupFilter) {
        const options: FindManyOptions<Group> = {
            relations: ['members'],
            select: {
                id: true,
                name: true,
                description: true,
                enabled: true,
                permissions: true,
                members: {
                    id: true,
                    username: true,
                    email: true,
                    avatar: true,
                    enabled: true,
                },
            },
        };
        return await this.groupRepo.paginate(filter, options);
    }
}
