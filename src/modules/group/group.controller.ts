import { Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Post, Put, Query, Req, Res } from '@nestjs/common';
import { ApiHeader, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

import { CreateGroupDTO } from './dto/create-group.dto';
import { GroupService } from './group.service';
import { Group, PERMISSION } from './group.entity';
import { RequirePermissions } from 'src/modules/auth/decorator/permissions.decorator';
import { UpdateGroupDTO } from './dto/update-group.dto';
import { Pagination } from 'src/common/pagination.interface';
import { GroupDTO } from './dto/group.dto';
import { GroupFilter } from 'src/common/generic-filter.entity';

@Controller('groups')
@ApiTags('Groups')
@ApiHeader({ name: 'Authorization' })
export class GroupController {
    constructor(private groupService: GroupService) {}

    @Get()
    @ApiResponse({ status: HttpStatus.OK, description: 'Successfully' })
    async findAll(@Req() req: Request, @Res() res: Response, @Query() filters: GroupFilter) {
        const groups: Pagination<Group> = await this.groupService.findAllPaginated(filters);

        return res.status(200).json({
            data: groups,
            message: 'All groups data',
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Get(':id')
    @RequirePermissions(PERMISSION.EDIT_GROUP)
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Cannot find group with provided id' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Successfully' })
    async findById(@Req() req: Request, @Res() res: Response, @Param('id', ParseIntPipe) id: number) {
        const savedGroup: GroupDTO = await this.groupService.findById(id);

        return res.status(200).json({
            data: savedGroup,
            message: `Get group info with id ${id}`,
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Post()
    @RequirePermissions(PERMISSION.EDIT_GROUP)
    @ApiResponse({ status: HttpStatus.OK, description: 'Create new group successfully' })
    async create(@Req() req: Request, @Res() res: Response, @Body() groupDto: CreateGroupDTO) {
        const createdGroup: GroupDTO = await this.groupService.create(groupDto);

        return res.status(200).json({
            data: createdGroup,
            message: 'Group is created successfully',
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }

    @Put(':id')
    @RequirePermissions(PERMISSION.EDIT_GROUP)
    @ApiResponse({ status: HttpStatus.OK, description: 'Update group successfully' })
    async update(
        @Req() req: Request,
        @Res() res: Response,
        @Body() groupDto: UpdateGroupDTO,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const updatedGroup: GroupDTO = await this.groupService.updateById(id, groupDto);

        return res.status(200).json({
            data: updatedGroup,
            message: `Group with id ${id} is updated successfully`,
            statusCode: HttpStatus.OK,
            requestedBy: req['user'].id,
        });
    }
}
