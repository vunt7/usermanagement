import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GroupController } from './group.controller';
import { GroupService } from './group.service';
import { GroupRepository } from './group.repository';
import { UserRepository } from '../user/user.repository';

@Module({
    controllers: [GroupController],
    providers: [GroupService, GroupRepository, UserRepository],
    imports: [],
    exports: [GroupService],
})
export class GroupModule {}
