import { Message } from 'src/modules/message/message.entity';
import { User } from 'src/modules/user/user.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('rooms')
export class Room {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar' })
    name: string;

    @Column({ type: 'boolean' })
    enabled: boolean = true;

    @ManyToMany(() => User, (user: User) => user.rooms, {
        cascade: ['insert', 'update'],
    })
    @JoinTable({
        name: 'room_user',
        joinColumn: {
            name: 'room_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'user_id',
            referencedColumnName: 'id',
        },
    })
    members?: User[];

    @OneToMany(() => Message, (message) => message.room, { cascade: ['remove'] })
    messages?: Message[];
}
