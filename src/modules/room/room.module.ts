import { Module } from '@nestjs/common';
import { RoomService } from './room.service';
import { RoomController } from './room.controller';
import { Room } from './room.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/modules/user/user.entity';
import { Message } from 'src/modules/message/message.entity';

@Module({
    providers: [RoomService],
    controllers: [RoomController],
    imports: [TypeOrmModule.forFeature([Room, User, Message])],
})
export class RoomModule {}
