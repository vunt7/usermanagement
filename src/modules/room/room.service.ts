import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Room } from './room.entity';
import { User } from 'src/modules/user/user.entity';
import { RoomDTO } from './dto/room.dto';
import { CreateRoomDTO } from './dto/create-room.dto';
import { UpdateRoomDTO } from './dto/update-room.dto';
import { plainToInstance } from 'class-transformer';

@Injectable()
export class RoomService {
    constructor(
        @InjectRepository(Room) private roomRepo: Repository<Room>,
        @InjectRepository(User) private userRepo: Repository<User>,
    ) {}

    async create(dto: CreateRoomDTO): Promise<RoomDTO> {
        const room = new Room();
        room.name = dto.name;
        const createdRoom: Room = await this.roomRepo.save(room);
        return plainToInstance(RoomDTO, createdRoom);
    }

    async update(id: number, dto: UpdateRoomDTO): Promise<RoomDTO> {
        const savedRoom: Room | null = await this.roomRepo.findOne({ where: { id } });
        if (!savedRoom) throw new NotFoundException(`Cannot find room with id ${id}`);
        savedRoom.name = dto.name;
        if (dto.members) {
            savedRoom.members = [];
            for (const userId of dto?.members) {
                const user: User | null = await this.userRepo.findOne({ where: { id: userId } });
                if (!user) throw new NotFoundException(`Cannot find user with id ${id}`);
                savedRoom.members.push(user);
            }
        }
        return plainToInstance(RoomDTO, this.roomRepo.save(savedRoom));
    }

    async findById(id: number): Promise<Room> {
        const savedRoom: Room | null = await this.roomRepo.findOne({
            where: { id },
            select: {
                members: {
                    password: false,
                    activation_key: false,
                    groups: false,
                    messages: false,
                    rooms: false,
                    reset_token: false,
                },
                messages: { author: { id: true, avatar: true, email: true, username: true } },
                id: true,
                name: true,
            },
        });
        if (!savedRoom) throw new NotFoundException(`Cannot find room with id ${id}`);
        return savedRoom;
    }
}
