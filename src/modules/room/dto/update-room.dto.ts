import { IsArray, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';

export class UpdateRoomDTO {
    @IsNumber()
    id: number;

    @IsOptional()
    @IsString()
    @MaxLength(100)
    name?: string;

    enabled: boolean = true;

    @IsOptional()
    @IsArray()
    @IsNumber({}, { each: true })
    members?: number[];
}
