import { IsString, MaxLength } from 'class-validator';

export class CreateRoomDTO {
    @IsString()
    @MaxLength(100)
    name: string;
}
