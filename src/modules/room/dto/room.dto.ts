import { IsArray, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';
import { Message } from 'src/modules/message/message.entity';
import { User } from 'src/modules/user/user.entity';

export class RoomDTO {
    @IsNumber()
    id: number;

    @IsString()
    @MaxLength(100)
    name: string;

    @IsArray()
    members: User[] = [];

    @IsArray()
    messages: Message[] = [];
}
