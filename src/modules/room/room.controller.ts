import { Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Post, Put, Req, Res } from '@nestjs/common';
import { RoomService } from './room.service';
import { Request, Response } from 'express';
import { ApiHeader, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from '../auth/decorator/permissions.decorator';
import { PERMISSION } from '../group/group.entity';
import { CreateRoomDTO } from './dto/create-room.dto';
import { identity } from 'lodash';
import { UpdateRoomDTO } from './dto/update-room.dto';

@Controller('rooms')
@ApiTags('Rooms')
@ApiHeader({ name: 'Authorization' })
export class RoomController {
    constructor(private readonly roomService: RoomService) {}

    @Post('/')
    @RequirePermissions(PERMISSION.MANAGE_ROOM)
    @ApiResponse({ status: HttpStatus.OK, description: 'Create new room chat successfully' })
    async create(@Req() req: Request, @Res() res: Response, @Body() roomDto: CreateRoomDTO) {
        const createdRoom = await this.roomService.create(roomDto);
        return res.status(200).json({
            data: createdRoom,
            statusCode: 200,
            requestedBy: req['user'].id,
        });
    }

    @Put(':id')
    async update(
        @Req() req: Request,
        @Res() res: Response,
        @Param('id', ParseIntPipe) id: number,
        @Body() updateDto: UpdateRoomDTO,
    ) {
        const updatedRoom = await this.roomService.update(id, updateDto);
        return res.status(200).json({
            data: updateDto,
            requestedBy: req['user'].id,
            statusCode: 200,
        });
    }
}
