import { OnModuleInit, UseGuards } from '@nestjs/common';
import { OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { WsGuard } from 'src/modules/auth/ws.guard';
import { MessageDTO } from '../message/dto/message.dto';
import { UserService } from '../user/user.service';

const BOT = 'CHAT_BOT';

@UseGuards(WsGuard)
@WebSocketGateway({ cors: { origin: 'http://localhost:3000' } })
export class EventsGateway implements OnModuleInit, OnGatewayDisconnect {
    constructor(private readonly userService: UserService) {}

    handleDisconnect(socket: Socket) {
        console.log('Before disconnect: ', socket.handshake.query);
    }
    @WebSocketServer()
    server: Server;

    onModuleInit() {
        this.server.on('connection', (socket: Socket) => {
            console.log(socket.handshake.query);
            // console.log(socket.id);
            // console.log(socket.connected);
        });
    }

    @SubscribeMessage('join_room')
    handleJoinRoom(socket: Socket, payload: { room: string }) {
        socket.join(payload.room);

        // alert the user that join the chat room
        // const message: MessageDTO = {content: }
        // socket.to(payload.room).emit('Join Room', { });
    }

    @SubscribeMessage('message')
    handleMessage(client: Socket, payload: any) {
        // console.log(client.id);
        console.log(client.id);
    }
}
