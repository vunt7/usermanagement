import { Module } from '@nestjs/common';

import { EventsGateway } from './events.gateway';
import { WsGuard } from 'src/modules/auth/ws.guard';
import { UserModule } from 'src/modules/user/user.module';

@Module({
    providers: [EventsGateway, WsGuard],
    imports: [UserModule],
})
export class EventsModule {}
