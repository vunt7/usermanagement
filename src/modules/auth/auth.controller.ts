import { Body, Controller, Get, HttpStatus, Post, Req, Res, UnauthorizedException } from '@nestjs/common';
import { Response, Request } from 'express';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtService } from '@nestjs/jwt';
import { Public } from './decorator/auth.decorator';

import { AuthService } from './auth.service';
import { SigninUserDTO } from 'src/modules/user/dto/signin-user.dto';
import { SignupUserDTO } from 'src/modules/user/dto/signup-user.dto';
import { EmailService } from 'src/utils/sendMail';
import { UserService } from 'src/modules/user/user.service';
import { User } from 'src/modules/user/user.entity';
import { jwtConstants } from 'src/config/jwt-config';
import { UserDTO } from '../user/dto/user.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly emailSender: EmailService,
        private readonly jwtService: JwtService,
        private readonly userService: UserService,
    ) {}

    @Public()
    @Post('login')
    @ApiResponse({ status: HttpStatus.OK, description: 'If log in success, server will return signed token' })
    @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Account was be disabled' })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Cannot find account or password is not match' })
    async signIn(@Res() res: Response, @Body() signInDto: SigninUserDTO) {
        const data: { accessToken: string; user: UserDTO } = await this.authService.signIn(
            signInDto.email,
            signInDto.password,
        );
        return res.status(200).json({
            data,
            statusCode: HttpStatus.OK,
        });
    }

    @Public() // set this route to public
    @Post('register')
    @ApiResponse({ status: HttpStatus.NOT_ACCEPTABLE, description: 'Account info were used in an other account' })
    @ApiResponse({
        status: HttpStatus.CREATED,
        description: 'Account was created successfully, check mail to enable account',
    })
    async signUp(@Res() res: Response, @Body() signUpDto: SignupUserDTO) {
        const key: string = await this.authService.signUp(signUpDto); // used for comparing with client send
        await this.emailSender.sendEmail(
            signUpDto.email,
            'Verify Email',
            `<h1>Hello, ${signUpDto.username}</h1>, we <a href="${process.env.URL_REACT_APP}">Codeforces</a> received request to sign up from you, please click on below link to continue sign up process!!! <br/><a href="${process.env.URL_REACT_APP}/verify-email?email=${signUpDto.email}&key=${key}">Verify Email</a>`,
        );
        return res.status(200).json({
            statusCode: HttpStatus.OK,
            message: `Register progress with email ${signUpDto.email} is success`,
        });
    }

    @Public()
    @Post('verify-email')
    @ApiResponse({ status: HttpStatus.NOT_ACCEPTABLE, description: 'Token is invalid' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Account is active, start to be available',
    })
    async verifyEmail(@Res() res: Response, @Body() { key, email }: { key: string; email: string }) {
        await this.userService.verifyUser({
            email,
            key,
        });
        return res.status(200).json({
            message: 'Done! Now you can log in to use this service',
            statusCode: HttpStatus.OK,
        });
    }

    @Public()
    @Post('/forgot-password')
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Email is invalid' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Send mail successfully with reset token',
    })
    async forgotPassword(@Res() res: Response, @Body() { email = '' }: { email: string }) {
        const user: User = await this.userService.findOneByEmail(email); // check if user exists
        const resetToken: string = await this.userService.forgotPassword(email); // generate new reset token
        //send mail to user
        await this.emailSender.sendEmail(
            email,
            'Reset Password?',
            `<h1>Hello, ${user.username}</h1>, we <a href="${process.env.URL_REACT_APP}">Codeforces</a> received request to reset password from you, please click on below link to continue forgot password process!!! <br/><a href="${process.env.URL_REACT_APP}/reset-password?email=${email}&token=${resetToken}">Reset Password</a>`,
        );
        return res.status(200).json({
            message: `Forgot password progress with email ${email} is success`,
            statusCode: HttpStatus.OK,
        });
    }

    @Public()
    @Post('/reset-password')
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Cannot reset password' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Reset password successfully with reset token',
    })
    async resetPassword(
        @Res() res: Response,
        @Body() { email = '', password = '', resetToken = '' }: { email: string; password: string; resetToken: string },
    ) {
        const result: boolean = await this.userService.resetPassword({ email, password, resetToken });
        if (!result) {
            // some errors occurred
            return res.status(HttpStatus.BAD_REQUEST).json({
                message: 'try again, some error occurred',
                statusCode: HttpStatus.BAD_REQUEST,
            });
        }
        return res.status(200).json({
            message: 'Progress is done, let log in!',
            statusCode: HttpStatus.OK,
        });
    }

    @Public()
    @Get()
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Token is invalid or expired' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Send user data back to the client successfully' })
    async getUserInfoByAccessToken(@Req() req: Request, @Res() res: Response) {
        const [type, token] = req.headers.authorization?.split(' ') ?? [];
        if (type !== 'Bearer') throw new UnauthorizedException('Invalid Bearer token');
        const payload = await this.jwtService.verifyAsync(token, {
            secret: jwtConstants.secret,
        });
        const user: UserDTO = await this.userService.findOne(payload?.id);
        return res.status(200).json({
            data: user,
            message: 'Get user info by access token successfully',
            statusCode: HttpStatus.OK,
            requestedBy: user.id,
        });
    }

    @Get('/logout')
    logout(@Res() res: Response) {
        return res.status(200).json({
            message: 'Logout successfully',
            statusCode: 200,
        });
    }
}
