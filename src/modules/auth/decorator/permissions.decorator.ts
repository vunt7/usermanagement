import { SetMetadata } from '@nestjs/common';
import { PERMISSION } from 'src/modules/group/group.entity';

export const PERMISSIONS_KEY = 'permissions';
export const RequirePermissions = (...permissions: PERMISSION[]) => SetMetadata(PERMISSIONS_KEY, permissions);
