import {
    BadRequestException,
    ForbiddenException,
    Injectable,
    NotFoundException,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { plainToInstance } from 'class-transformer';

import { SignupUserDTO } from 'src/modules/user/dto/signup-user.dto';
import { User } from 'src/modules/user/user.entity';
import { UserService } from 'src/modules/user/user.service';
import { UserDTO } from '../user/dto/user.dto';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UserService,
        private jwtService: JwtService,
    ) {}

    // generate random string
    generateString(length: number): string {
        // declare all characters
        const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let result: string = '';
        const charactersLength: number = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    async signIn(email: string, password: string): Promise<{ accessToken: string; user: UserDTO }> {
        const user: User = await this.usersService.findOneByEmail(email); // find account
        if (!user) throw new NotFoundException(`User ${email} not found`);
        if (!user?.enabled) throw new ForbiddenException('Account has been disabled');
        const isMatch: boolean = await bcrypt.compare(password, user.password); // password is correct?
        if (!isMatch) {
            // password is not correct or account is disabled
            throw new UnauthorizedException('Invalid credentials');
        }
        const payload = { id: user.id, username: user.username }; // data will be signed
        return {
            accessToken: await this.jwtService.signAsync(payload), // return jwt back to client
            user: plainToInstance(UserDTO, user),
        };
    }

    // return signed token
    async signUp(userDto: SignupUserDTO): Promise<string> {
        const accountExists: boolean = await this.usersService.emailExists(userDto.email); // default email is unique, username can be same
        if (accountExists) throw new BadRequestException(`${userDto.email} has been registered before`);
        const newUser = await this.usersService.create(userDto); // create new account
        return newUser.activation_key;
    }
}
