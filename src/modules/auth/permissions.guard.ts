import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { PERMISSIONS_KEY } from './decorator/permissions.decorator';

import { PERMISSION } from 'src/modules/group/group.entity';

@Injectable()
export class PermissonsGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const requiredPermissions = this.reflector.getAllAndOverride<PERMISSION[]>(PERMISSIONS_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (!requiredPermissions) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const user = request.user; // get user from request, user is assigned at authentication guard before
        const userId = user.id;
        const requestedId = request.params.id;
        // if (userId === 1) return true; // by default user with id = 1 is administrator
        if (userId === +requestedId && request.route.path.startsWith('/users')) {
            // if user try to edit your own infomation, accepted
            return true;
        }
        if (
            request.route.path === '/groups/:id' &&
            request.route.methods?.get === true &&
            user.groups?.has(+requestedId)
        ) {
            // user in the group can see all the member of group
            return true;
        }
        if (
            request.route.path === '/groups/:id' &&
            request.route.methods?.put === true &&
            user.groups?.has(+requestedId)
        ) {
            // user in the group cannot change group properties
            return false;
        }
        return requiredPermissions.some((permission) => user.permissions?.has(permission)); // check user has required permissions
    }
}
