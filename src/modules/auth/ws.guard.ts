import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Socket } from 'socket.io';
import { jwtConstants } from 'src/config/jwt-config';

import { UserService } from 'src/modules/user/user.service';

@Injectable()
export class WsGuard implements CanActivate {
    constructor(
        private jwtService: JwtService,
        private userService: UserService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const client: Socket = context.switchToWs().getClient<Socket>();
        const token: unknown = client.handshake?.query?.accessToken;
        console.log('Here: ', token);
        if (typeof token !== 'string' || !token) {
            throw new UnauthorizedException();
        }
        try {
            const payload = await this.jwtService.verifyAsync(token, {
                secret: jwtConstants.secret,
            });
            await this.userService.findOne(payload.id);
        } catch {
            throw new UnauthorizedException();
        }
        return true;
    }
}
