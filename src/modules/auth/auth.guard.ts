import { CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
import { Cache, CACHE_MANAGER } from '@nestjs/cache-manager';

import { IS_PUBLIC_KEY } from './decorator/auth.decorator';
import { UserService } from 'src/modules/user/user.service';
import { PERMISSION } from 'src/modules/group/group.entity';
import { jwtConstants } from 'src/config/jwt-config';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private jwtService: JwtService,
        private reflector: Reflector,
        private userService: UserService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (isPublic) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        if (!token) {
            throw new UnauthorizedException();
        }
        try {
            const payload = await this.jwtService.verifyAsync(token, {
                secret: jwtConstants.secret,
            });
            const user = await this.userService.findOne(payload.id);

            // reset cache when user logout
            if (request.route.path === '/logout' && request.route.methods?.get === true) {
                await this.cacheManager.del(`${user.id}`);
                return true;
            }
            // here we store user id is key and value is an object with two fields permissions and groups
            const permissions = new Set<PERMISSION>(); // using set to store all permissions of user
            const groups = new Set<number>();
            //using cache to store user's permissions and groups they blong to
            const userCache: { permissions: Set<PERMISSION>; groups: Set<number> } = await this.cacheManager.get(
                `${user.id}`,
            );

            if (!userCache) {
                // cache is expired
                // loop to get all permissions of specified user
                for (const group of user.groups) {
                    groups.add(group.id);
                    if (group.enabled) for (const permission of group.permissions) permissions.add(permission);
                }
                payload.permissions = permissions;
                payload.groups = groups;

                // store to cache
                await this.cacheManager.set(`${user.id}`, { permissions, groups }, 1000000);
            } else {
                payload.permissions = userCache.permissions;
                payload.groups = userCache.groups;
            }
            request['user'] = payload; // used for authorization
        } catch {
            throw new UnauthorizedException();
        }
        return true;
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}
