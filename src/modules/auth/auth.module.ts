import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { APP_GUARD } from '@nestjs/core';

import { AuthGuard } from './auth.guard';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModule } from 'src/modules/user/user.module';
import { PermissonsGuard } from './permissions.guard';
import { EmailService } from 'src/utils/sendMail';
import { jwtConstants } from 'src/config/jwt-config';

@Module({
    controllers: [AuthController],
    imports: [
        JwtModule.register({
            global: true,
            secret: jwtConstants.secret,
            signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
        UserModule,
    ],
    providers: [
        AuthService,
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        },
        {
            provide: APP_GUARD,
            useClass: PermissonsGuard,
        },
        // {
        //     provide: APP_GUARD,
        //     useClass: WsGuard,
        // },
        EmailService, // inject email service for sending email
    ],
    exports: [AuthService],
})
export class AuthModule {}
