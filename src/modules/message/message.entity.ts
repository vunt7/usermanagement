import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { User } from 'src/modules/user/user.entity';
import { Room } from '../room/room.entity';

@Entity('messages')
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 1000 })
    content: string;

    @ManyToOne(() => User, (user: User) => user.messages, {
        onDelete: 'CASCADE',
    })
    author: User;

    @ManyToOne(() => Room, (room: Room) => room.messages, {
        onDelete: 'CASCADE',
    })
    room: Room;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}
