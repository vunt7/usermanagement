import { IsNumber, IsObject, IsOptional, IsString, MaxLength } from 'class-validator';

export class MessageDTO {
    @IsOptional()
    @IsNumber()
    id?: number;

    @IsString()
    @MaxLength(1000)
    content: string;

    @IsOptional()
    @IsNumber()
    author?: number;

    @IsObject()
    @IsNumber()
    room?: number;

    @IsObject()
    createdAt?: Date;
}
