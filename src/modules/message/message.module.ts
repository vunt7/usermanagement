import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';
import { Message } from './message.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/modules/user/user.entity';
import { Room } from '../room/room.entity';

@Module({
    controllers: [MessageController],
    providers: [MessageService],
    imports: [TypeOrmModule.forFeature([Message, User, Room])],
})
export class MessageModule {}
