import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from 'src/modules/user/user.entity';
import { Message } from './message.entity';
import { Room } from '../room/room.entity';
import { MessageDTO } from './dto/message.dto';

@Injectable()
export class MessageService {
    constructor(
        @InjectRepository(Message) private messageRepo: Repository<Message>,
        @InjectRepository(User) private userRepo: Repository<User>,
        @InjectRepository(Room) private roomRepo: Repository<Room>,
    ) {}

    async create(dto: MessageDTO): Promise<Message> {
        const message = new Message();
        message.content = dto.content;
        const author: User | null = await this.userRepo.findOne({ where: { id: dto.author } });
        if (!author) throw new NotFoundException(`Cannot find user with id ${dto.author}`);
        const room: Room | null = await this.roomRepo.findOne({ where: { id: dto.room } });
        if (!room) throw new NotFoundException(`Cannot find room with id ${dto.room}`);

        message.author = author;
        message.room = room;
        return this.messageRepo.save(message);
    }

    async update(id: number, dto: MessageDTO): Promise<Message> {
        const message: Message | null = await this.messageRepo.findOne({ where: { id } });
        if (!message) throw new NotFoundException(`Cannot find message with id ${id}`);
        message.content = dto.content;
        return this.messageRepo.save(message);
    }

    async findById(id: number): Promise<Message> {
        const message: Message | null = await this.messageRepo.findOne({ where: { id } });
        if (!message) throw new NotFoundException(`Cannot find message with id ${id}`);
        return message;
    }

    async getLatestMessages(roomId: number): Promise<Message[]> {
        return this.messageRepo.find({
            where: { room: { id: roomId } },
            order: { createdAt: 'DESC' },
            take: 100,
        });
    }
}
