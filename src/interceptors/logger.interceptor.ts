import { CallHandler, ExecutionContext, Logger, NestInterceptor } from '@nestjs/common';
import { Observable, catchError, tap, throwError } from 'rxjs';

export class LoggerInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const req = context.switchToHttp().getRequest();
        const method = req.method;
        const url = req.url;
        console.log(`Before... Method: ${method}, URL: ${url}`);
        const now = Date.now();
        return next.handle().pipe(
            // log response, execution time, path, method, statusCode
            tap(() => {
                const res = context.switchToHttp().getResponse();
                Logger.log(`After... ${method} ${url} ${Date.now() - now}ms at ${context.getClass().name}`);
                Logger.log(`Status: ${res.statusCode}`);
            }),
            // catch exception
            catchError((err) => {
                Logger.log(`After... ${err}`);
                return throwError(err);
            }),
        );
    }
}
