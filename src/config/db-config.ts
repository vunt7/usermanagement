import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';

import { Group } from 'src/modules/group/group.entity';
import { Message } from 'src/modules/message/message.entity';
import { Room } from 'src/modules/room/room.entity';
import { User } from 'src/modules/user/user.entity';

export const config: TypeOrmModuleAsyncOptions = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: configService.get('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        entities: [User, Group, Room, Message],
        synchronize: true,
        // dropSchema: true,
    }),
};
