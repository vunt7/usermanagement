import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.enableCors(); // enable CORS support
    app.useGlobalPipes(new ValidationPipe({ transform: true })); // enable validate pipe global
    const config = new DocumentBuilder()
        .addBearerAuth()
        .setTitle('User Management Api')
        .setDescription('The user management API description')
        .setVersion('1.0')
        .addTag('demo')
        .build();
    const options: SwaggerDocumentOptions = {
        operationIdFactory: (controllerKey: string, methodKey: string) => methodKey,
    };
    const document = SwaggerModule.createDocument(app, config, options);
    SwaggerModule.setup('api', app, document);
    await app.listen(5000);
}
bootstrap();
