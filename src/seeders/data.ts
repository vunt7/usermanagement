import { CreateGroupDTO } from 'src/modules/group/dto/create-group.dto';
import { PERMISSION } from 'src/modules/group/group.entity';
import { User } from 'src/modules/user/user.entity';

export const GROUP_LIST: CreateGroupDTO[] = [
    {
        name: 'administrator',
        permissions: [PERMISSION.EDIT_ACCOUNT, PERMISSION.EDIT_GROUP],
        enabled: true,
        description: 'Full control role',
        members: [1],
    },
    {
        name: 'editor',
        permissions: [PERMISSION.EDIT_ACCOUNT],
        enabled: true,
        description: 'Edit account permissions',
        members: [],
    },
    {
        name: 'user',
        permissions: [],
        enabled: true,
        description: 'Normal user',
        members: [],
    },
];

export const USER_LIST: User[] = [
    {
        username: 'Admin',
        email: 'admin@gmail.com',
        password: 'admin',
        enabled: true,
        groups: [],
        rooms: [],
        messages: [],
        avatar: 'public\\img\\default\\default-avatar.jpg',
        id: 1,
    },
    {
        username: 'User',
        email: 'user@gmail.com',
        password: 'user',
        enabled: true,
        groups: [],
        rooms: [],
        messages: [],
        avatar: 'public\\img\\default\\default-avatar.jpg',
        id: 2,
    },
    {
        username: 'Editor',
        email: 'editor@gmail.com',
        password: 'editor',
        enabled: true,
        groups: [],
        rooms: [],
        messages: [],
        avatar: 'public\\img\\default\\default-avatar.jpg',
        id: 3,
    },
];
