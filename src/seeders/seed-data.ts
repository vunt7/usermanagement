import * as bcrypt from 'bcrypt';

import { Group } from 'src/modules/group/group.entity';
import { User } from 'src/modules/user/user.entity';
import { DataSource } from 'typeorm';
import { GROUP_LIST, USER_LIST } from './data';

export async function seedData(dataSource: DataSource): Promise<void> {
    const userRepository = dataSource.getRepository(User);
    const groupRepository = dataSource.getRepository(Group);

    const userList = USER_LIST;
    const salt = await bcrypt.genSalt();
    for (const user of userList) {
        user.password = await bcrypt.hash(user.password, salt);
        let _user = userRepository.create(user as unknown as User);
        await userRepository.save(_user);
    }

    const groupList = GROUP_LIST;
    for (const dto of groupList) {
        const group = new Group();
        group.name = dto.name;
        group.permissions = dto.permissions;
        group.description = dto.description;
        group.enabled = dto.enabled;
        group.members = [];
        for (const memberId of dto.members) {
            const mem = await userRepository.findOne({ where: { id: memberId } });
            if (mem) group.members.push(mem);
        }
        await groupRepository.save(group);
    }
}
