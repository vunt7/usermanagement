// external
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { CacheModule } from '@nestjs/cache-manager';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';

// user
import { UserModule } from './modules/user/user.module';

// group
import { GroupModule } from './modules/group/group.module';

// auth
import { AuthModule } from './modules/auth/auth.module';

// exception filters
import { AllExceptionsFilter } from './common/all-exceptions.filter';

// utils
import { EmailService } from './utils/sendMail';
import { LoggerInterceptor } from './interceptors/logger.interceptor';
import { MessageModule } from './modules/message/message.module';
import { EventsModule } from './modules/events/events.module';
import { RoomModule } from './modules/room/room.module';
import { config } from './config/db-config';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }), // enable .env file
        TypeOrmModule.forRootAsync(config),
        ServeStaticModule.forRoot({
            // absoluth path
            rootPath: join(__dirname, '..'),
        }),
        CacheModule.register({ isGlobal: true }), // for caching,
        UserModule,
        GroupModule,
        AuthModule,
        RoomModule,
        MessageModule,
        EventsModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        {
            provide: APP_FILTER, // using all exceptions filter globally
            useClass: AllExceptionsFilter,
        },
        { provide: APP_INTERCEPTOR, useClass: LoggerInterceptor }, // using logger to track requests to server
        EmailService, // sending email
    ],
})
export class AppModule {
    constructor() {
        console.log('Conect to db successfully ...');
    }
}
